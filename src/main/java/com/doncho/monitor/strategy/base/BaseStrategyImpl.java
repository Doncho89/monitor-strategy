package com.doncho.monitor.strategy.base;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@XmlAccessorType(XmlAccessType.FIELD)
public class BaseStrategyImpl implements IBaseStrategy
{

    @XmlAttribute(name = "name")
    private String name;

    @XmlElement(name = "active")
    private boolean active;

    @XmlElement(name = "all-logical-systems")
    private String allLogicalSystems;

    @XmlElement(name = "monitor-type")
    private String monitorType;


    public BaseStrategyImpl()
    {
        //Default
    }


    public BaseStrategyImpl(String name, boolean active, String allLogicalSystems, String monitorType)
    {
        this.name = name;
        this.active = active;
        this.allLogicalSystems = allLogicalSystems;
        this.monitorType = monitorType;
    }


    @Override
    public void addBaseValuesAll(String name, Boolean active, String AllLs, String monitorType)
    {
        this.name = name;
        this.active = active;
        this.monitorType = monitorType;

        if(null != AllLs)
        {
            this.allLogicalSystems = AllLs;
        }
    }

    @Override
    public void addBaseValuesNotAllLS(String name, Boolean active, String monitorType)
    {
        this.name = name;
        this.active = active;
        this.monitorType = monitorType;
    }


    @Override
    public void setName(String name)
    {
        this.name = name;
    }


    @Override
    public String getName()
    {
        return name;
    }

    @Override
    public void setActive(Boolean active)
    {
        this.active = active;
    }

    @Override
    public Boolean getActive()
    {
        return active;
    }

    @Override
    public void setAllLS(String allLS)
    {
        this.allLogicalSystems = allLS;
    }

    @Override
    public String getAllLS()
    {
        return allLogicalSystems;
    }

    @Override
    public String getMonitorType()
    {
        return monitorType;
    }

    @Override
    public void setMonitorType(String monitorType)
    {
        this.monitorType = monitorType;
    }


    @Override public String toString()
    {
        return "BaseStrategyImpl{" + "name='" + name +
               '\'' + ", active=" + active +
               ", allLogicalSystems=" + allLogicalSystems +
               ", monitorType='" + monitorType + '\'' + '}';
    }
}
