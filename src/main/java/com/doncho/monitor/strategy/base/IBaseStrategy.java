package com.doncho.monitor.strategy.base;


public interface IBaseStrategy
{
    void addBaseValuesAll(String name, Boolean active, String AllLs, String monitorTypes);

    void addBaseValuesNotAllLS(String name, Boolean active, String monitorTypes);

    void setName(String name);

    String getName();

    void setActive(Boolean active);

    Boolean getActive();

    void setAllLS(String AllLs);

    String getAllLS();

    String getMonitorType();

    void setMonitorType(String monitorType);
}
