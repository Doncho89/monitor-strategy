package com.doncho.monitor.strategy.context;


import com.doncho.monitor.models.BusinessProcessDurationModel;
import com.doncho.monitor.models.BusinessProcessModel;
import com.doncho.monitor.strategy.adapter.AdapterStrategyImpl;
import com.doncho.monitor.strategy.adapter.IAdapterStrategy;
import com.doncho.monitor.strategy.base.BaseStrategyImpl;
import com.doncho.monitor.strategy.businessprocess.IProcessStrategy;
import com.doncho.monitor.strategy.businessprocess.ProcessStrategyImpl;
import com.doncho.monitor.strategy.businessprocess.ProcessesModel;
import com.doncho.monitor.strategy.services.IServicesStrategy;
import com.doncho.monitor.strategy.services.ServicesStrategyImpl;
import com.doncho.monitor.strategy.settings.ISettingsStrategy;
import com.doncho.monitor.strategy.settings.SettingsParent;
import com.doncho.monitor.strategy.settings.SettingsStrategyImpl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;


@XmlAccessorType(XmlAccessType.NONE)
@XmlRootElement(name="alerting-monitor")
public class MonitorContext extends BaseStrategyImpl
{
    @XmlElement(name="adapters", type = AdapterStrategyImpl.class)
    private IAdapterStrategy adapterStrategy;

    @XmlElement(name="processes", type = ProcessStrategyImpl.class)
    private IProcessStrategy processStrategy;

    @XmlElement(name = "monitor-settings", type = SettingsParent.class)
    private SettingsParent settingsStrategy;

    @XmlElement(name="services", type = ServicesStrategyImpl.class)
    private IServicesStrategy servicesStrategy;


    public MonitorContext()
    {
        //Default
    }


    public void setSettingsStrategy(SettingsParent settingsStrategyArg)
    {
        this.settingsStrategy = settingsStrategyArg;
    }

    public void setAdapterStrategy(AdapterStrategyImpl adapterStrategyArg)
    {
        this.adapterStrategy = adapterStrategyArg;
    }

    public void setProcessStrategy(ProcessStrategyImpl processStrategyArg)
    {
        this.processStrategy = processStrategyArg;
    }

    public void setServicesStrategy(ServicesStrategyImpl servicesStrategyArg)
    {
        this.servicesStrategy = servicesStrategyArg;
    }


    /**
     * SETTERS
     */
    public void setBase(String name, boolean active, String allLogicalSystems, String monitorType)
    {
        addBaseValuesAll(name, active, allLogicalSystems, monitorType);
    }


    public void setSettings(String attribute, String value, String timeRange, String properties, String instanceId)
    {
        settingsStrategy.addSettings(attribute, value, timeRange, properties, instanceId);
    }


    public void setAdapters (List<String> adapters)
    {
        adapterStrategy.addRelatedAdapters(adapters);
    }


    public void setProcesses(List<ProcessesModel> processes)
    {
        processStrategy.addRelatedProcesses(processes);
    }


    public void setServices(List<String> services)
    {
        servicesStrategy.addRelatedServices(services);
    }


    /**
     * GETTERS
     */
    @XmlTransient
    public IAdapterStrategy getAdapterStrategy()
    {
        return adapterStrategy;
    }


    @XmlTransient
    public IProcessStrategy getProcessStrategy()
    {
        return processStrategy;
    }


    @XmlTransient
    public ISettingsStrategy getSettingsStrategy()
    {
        return settingsStrategy.getSettingsStrategy();
    }


    @XmlTransient
    public IServicesStrategy getServicesStrategy()
    {
        return servicesStrategy;
    }


    @Override public String toString()
    {
        return "MonitorContext: " + System.lineSeparator() +
               "adapterStrategy=" + adapterStrategy + System.lineSeparator() +
               "processStrategy=" + processStrategy + System.lineSeparator() +
               "settingsStrategy=" + settingsStrategy + System.lineSeparator() +
               "servicesStrategy=" + servicesStrategy;
    }

}
