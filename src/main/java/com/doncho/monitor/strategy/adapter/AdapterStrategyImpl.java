package com.doncho.monitor.strategy.adapter;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
public class AdapterStrategyImpl implements IAdapterStrategy
{
    @XmlElement(name = "adapter")
    private List<String> adapters;


    public AdapterStrategyImpl()
    {
        //Default
    }


    public AdapterStrategyImpl(List<String> adapters)
    {
        this.adapters = adapters;
    }


    @Override
    public void addRelatedAdapters(List<String> relatedAdaptersIds)
    {
        this.adapters = relatedAdaptersIds;

    }


    @Override
    @XmlTransient
    public List<String> getRelatedAdapters()
    {
        return adapters;
    }


    @Override

    public String toString()
    {
        return "AdapterStrategyImpl{" + adapters +'}';
    }
}
