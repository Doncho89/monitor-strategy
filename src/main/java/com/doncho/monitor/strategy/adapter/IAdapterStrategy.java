package com.doncho.monitor.strategy.adapter;


import java.util.List;


public interface IAdapterStrategy
{
    void addRelatedAdapters(List<String> relatedAdaptersIds);

    List<String> getRelatedAdapters();
}
