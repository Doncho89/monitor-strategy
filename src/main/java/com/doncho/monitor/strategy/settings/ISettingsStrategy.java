package com.doncho.monitor.strategy.settings;


import java.io.Serializable;


public interface ISettingsStrategy extends Serializable
{
    void addSettings(String attribute, String value, String timeRange, String properties, String instanceId);

    SettingsStrategyImpl getSettings();
}
