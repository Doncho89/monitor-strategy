package com.doncho.monitor.strategy.settings;


import com.doncho.monitor.models.AdapterDurationModel;
import com.doncho.monitor.models.BusinessProcessDurationModel;
import com.doncho.monitor.models.BusinessProcessModel;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElements;
import javax.xml.bind.annotation.XmlTransient;


@XmlAccessorType(XmlAccessType.FIELD)
public class SettingsParent implements ISettingsStrategy
{
    @XmlElement(name = "instance-id", defaultValue = "*")
    private String instanceId;

    @XmlElements({@XmlElement(name = "business-process-duration", nillable = true, type = BusinessProcessDurationModel.class),
                  @XmlElement(name = "business-process", nillable = true, type = BusinessProcessModel.class),
                  @XmlElement(name = "adapter-duration", nillable = true, type = AdapterDurationModel.class)})
    private SettingsStrategyImpl settingsStrategy;


    public SettingsParent()
    {
        //Default
    }


    public SettingsParent(SettingsStrategyImpl settingsStrategy)
    {
        this.settingsStrategy = settingsStrategy;
    }


    @XmlTransient
    public String getInstanceId()
    {
        return instanceId;
    }


    public void setInstanceId(String instanceId)
    {
        this.instanceId = instanceId;
    }


    @XmlTransient
    public SettingsStrategyImpl getSettingsStrategy()
    {
        return settingsStrategy.getSettings();
    }


    public void setSettingsStrategy(SettingsStrategyImpl settingsStrategy)
    {
        this.settingsStrategy = settingsStrategy;
    }


    @Override
    public void addSettings(String attribute, String value, String timeRange, String properties, String instanceId)
    {
        setInstanceId(instanceId);
        settingsStrategy.addSettings(attribute, value, timeRange, properties, instanceId);
    }


    @Override
    public SettingsStrategyImpl getSettings()
    {
        return settingsStrategy;
    }


    @Override public String toString()
    {
        return "SettingsParent:" + System.lineSeparator() +
               "instanceId='" + instanceId + '\'' + System.lineSeparator() +
               "settingsStrategy=" + settingsStrategy;
    }
}
