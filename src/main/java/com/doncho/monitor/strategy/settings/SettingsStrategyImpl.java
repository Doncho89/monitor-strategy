package com.doncho.monitor.strategy.settings;


import javax.xml.bind.annotation.XmlTransient;


public abstract class SettingsStrategyImpl implements ISettingsStrategy
{

    private String cAttribute;

    private String cValue;

    private String cTimeRange;

    private String cProperties;


    public SettingsStrategyImpl()
    {
        // Default
    }


    public SettingsStrategyImpl(String cAttributeArg, String cValueArg, String cTimeRangeArg, String cPropertiesArg)
    {
       this.cAttribute = cAttributeArg;
       this.cValue = cValueArg;
       this.cTimeRange = cTimeRangeArg;
       this.cProperties = cPropertiesArg;
    }


    @Override
    public void addSettings(String attribute, String value, String timeRange, String properties, String instanceId)
    {
        this.cAttribute = attribute;
        this.cValue = value;
        this.cTimeRange = timeRange;
        this.cProperties = properties;
    }


    @Override
    public SettingsStrategyImpl getSettings()
    {
        //TODO based on concrete business process monitor type
        //SettingsStrategyModel settingsStrategy = new SettingsStrategyModel("AverageDurationBPInstances", "60", "32005", "");

        return this;
    }


    public String getAttribute()
    {
        return cAttribute;
    }


    @XmlTransient
    public void setAttribute(String cAttribute)
    {
        this.cAttribute = cAttribute;
    }


    public String getValue()
    {
        return cValue;
    }


    @XmlTransient
    public void setValue(String cValue)
    {
        this.cValue = cValue;
    }


    public String getTimeRange()
    {
        return cTimeRange;
    }


    @XmlTransient
    public void setTimeRange(String cTimeRange)
    {
        this.cTimeRange = cTimeRange;
    }


    public String getProperties()
    {
        return cProperties;
    }


    @XmlTransient
    public void setProperties(String cProperties)
    {
        this.cProperties = cProperties;
    }


    @Override public String toString()
    {
        return "SettingsStrategyImpl{" + "cAttribute='" + cAttribute +
               '\'' + ", cValue='" + cValue +
               '\'' + ", cTimeRange='" + cTimeRange
               + '\'' + ", cProperties='" + cProperties + '\'' + '}';
    }
}
