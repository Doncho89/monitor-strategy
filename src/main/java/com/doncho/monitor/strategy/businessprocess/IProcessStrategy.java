package com.doncho.monitor.strategy.businessprocess;


import java.util.List;


public interface IProcessStrategy
{
    void addRelatedProcesses(List<ProcessesModel> relatedProcessIds);

    List<ProcessesModel> getRelatedBusinessProcesses();
}
