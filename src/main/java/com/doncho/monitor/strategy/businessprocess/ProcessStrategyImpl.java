package com.doncho.monitor.strategy.businessprocess;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlElementWrapper;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
public class ProcessStrategyImpl implements IProcessStrategy
{
    @XmlElement(name="process")
    private List<ProcessesModel> processTemplates;


    public ProcessStrategyImpl()
    {
        //Default
    }


    public ProcessStrategyImpl(List<ProcessesModel> processTemplates)
    {
        this.processTemplates = processTemplates;
    }


    @Override
    public void addRelatedProcesses(List<ProcessesModel> relatedProcessIds)
    {
        this.processTemplates = relatedProcessIds;
    }


    @Override
    @XmlTransient
    public List<ProcessesModel> getRelatedBusinessProcesses()
    {
        return processTemplates;
    }


    @Override public String toString()
    {
        return "ProcessStrategyImpl{" +
               "processTemplates=" + processTemplates + '}';
    }
}
