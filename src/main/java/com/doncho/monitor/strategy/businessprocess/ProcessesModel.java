package com.doncho.monitor.strategy.businessprocess;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlTransient;
import javax.xml.bind.annotation.XmlValue;


@XmlAccessorType(XmlAccessType.FIELD)
public class ProcessesModel
{
    @XmlAttribute(name = "href")
    private String href;


    public ProcessesModel()
    {
        //
    }


    public ProcessesModel(String href)
    {
        this.href = href;
    }


    @XmlTransient
    public String getHref()
    {
        return href;
    }


    public void setHref(String href)
    {
        this.href = href;
    }

}
