package com.doncho.monitor.strategy.services;


import java.util.List;


public interface IServicesStrategy
{
    void addRelatedServices(List<String> relatedServicesIds);

    List<String> getRelatedServices();
}
