package com.doncho.monitor.strategy.services;


import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;
import java.util.List;


@XmlAccessorType(XmlAccessType.FIELD)
public class ServicesStrategyImpl implements IServicesStrategy
{
    @XmlElement(name="service")
    private List<String> services;


    public ServicesStrategyImpl()
    {
        //Default
    }


    public ServicesStrategyImpl(List<String> services)
    {
        this.services = services;
    }


    @Override
    public void addRelatedServices(List<String> relatedServicesIds)
    {
        this.services = relatedServicesIds;
    }


    @Override
    @XmlTransient
    public List<String> getRelatedServices()
    {
        return services;
    }


    @Override public String toString()
    {
        return "ServicesStrategyImpl{" + "services=" + services + '}';
    }
}
