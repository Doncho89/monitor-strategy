package com.doncho.monitor.strategy;


import com.doncho.monitor.strategy.businessprocess.ProcessesModel;

import java.util.ArrayList;
import java.util.List;


public abstract class MonitorDataUtil
{
    public static final List<String> adapters     = new ArrayList<>();
    public static final List<String> processes    = new ArrayList<>();
    public static final List<String> services     = new ArrayList<>();

    private MonitorDataUtil ()
    {
        // Hide constructor
    }

    static
    {
        adapters.add("AS2 Client");
        adapters.add("AS24 Client");
        adapters.add("Amazon S3 Client");

        processes.add("https://balamdzhievd02:14000/api/process-repo/processes/a8a4b680-212b-11ec-ade3-d291c0a8017a");
        processes.add("https://balamdzhievd02:14000/api/process-repo/processes/5b941260-2125-11ec-ade3-d291c0a8017a");
        processes.add("https://balamdzhievd02:14000/api/process-repo/processes/fc872940-211c-11ec-ade3-d291c0a8017a");

        services.add("Command line service");
        services.add("FILE Service");
        services.add("Mail service");
    }


    public static List<ProcessesModel> adaptProcesses()
    {
        List<ProcessesModel> resultModel = new ArrayList<>();
        for (String processURI : processes)
        {
            resultModel.add(new ProcessesModel(processURI));
        }

        return resultModel;
    }
}
