package com.doncho.monitor.models;


import com.doncho.monitor.strategy.settings.SettingsStrategyImpl;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


public class AdapterDurationModel extends SettingsStrategyImpl
{
    /** field <code>serialVersionUID</code> */
    private static final long serialVersionUID = -6166150153248762210L;

    @XmlElement(name = "calculate-duration-for-last", defaultValue = "1000", type = Integer.class)
    private int calculateDurationForLast;

    @XmlElement(name = "average-duration-limit", defaultValue = "60", type = Integer.class)
    private int averageDurationLimit;

    @XmlElement(name = "properties", defaultValue = "", type = String.class)
    private String propertiesAdapter;


    public AdapterDurationModel()
    {
        //Default
    }


    @XmlTransient
    public int getCalculateDurationForLast()
    {
        return calculateDurationForLast;
    }


    public void setCalculateDurationForLast(int calculateDurationForLast)
    {
        this.calculateDurationForLast = calculateDurationForLast;
    }


    @XmlTransient
    public int getAverageDurationLimit()
    {
        return averageDurationLimit;
    }


    public void setAverageDurationLimit(int averageDurationLimit)
    {
        this.averageDurationLimit = averageDurationLimit;
    }


    @XmlTransient
    public String getPropertiesAdapter()
    {
        return propertiesAdapter;
    }


    public void setPropertiesAdapter(String propertiesAdapter)
    {
        this.propertiesAdapter = propertiesAdapter;
    }


    @Override
    public void addSettings(String attribute, String value, String timeRange, String properties, String instanceId)
    {
        //TODO any business logic for adapter duration
        setCalculateDurationForLast(Integer.parseInt(timeRange));
        setAverageDurationLimit(Integer.parseInt(value));
        setPropertiesAdapter(properties);

        super.addSettings(attribute, value, timeRange, properties, instanceId);
    }


    @Override
    public SettingsStrategyImpl getSettings()
    {
        //TODO any business logic for adapter duration

        if(null == getAttribute() || getAttribute().isEmpty())
        {
            setAttribute("AverageDurationAdapter");// TODO different for each monitor type
        }
        if(null == getValue() || getValue().isEmpty())
        {
            setValue(String.valueOf(getAverageDurationLimit()));// TODO different for each monitor type
        }
        if(null == getTimeRange() || getTimeRange().isEmpty())
        {
            setTimeRange(String.valueOf(getCalculateDurationForLast()));// TODO different for each monitor type
        }
        if(null == getProperties() || getProperties().isEmpty())
        {
            setProperties(getPropertiesAdapter());// TODO different for each monitor type - in this case is empty
        }

        return super.getSettings();
    }

}
