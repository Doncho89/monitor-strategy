package com.doncho.monitor.models;


import com.doncho.monitor.strategy.settings.SettingsStrategyImpl;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlTransient;


@XmlAccessorType(XmlAccessType.FIELD)
public class BusinessProcessDurationModel extends SettingsStrategyImpl
{
    /** field <code>serialVersionUID</code> */
    private static final long serialVersionUID = -6166250153248762210L;

    @XmlElement(name = "average-duration-limit", defaultValue = "60", type = Integer.class)
    private int averageDuration;

    @XmlElement(name = "calculate-duration-for-last", defaultValue = "1000", type = Integer.class)
    private int calculateDuration;

    @XmlElement(name = "process-observing-state", defaultValue = "final", type = String.class)
    private String processObserving;


    public BusinessProcessDurationModel()
    {
        // Default constructor for JAXB
    }


    @XmlTransient
    public int getAverageDuration()
    {
        return averageDuration;
    }


    public void setAverageDuration(int averageDuration)
    {
        this.averageDuration = averageDuration;
    }


    @XmlTransient
    public int getCalculateDuration()
    {
        return calculateDuration;
    }


    public void setCalculateDuration(int calculateDuration)
    {
        this.calculateDuration = calculateDuration;
    }


    @XmlTransient
    public String getProcessObserving()
    {
        return processObserving;
    }


    public void setProcessObserving(String processObserving)
    {
        this.processObserving = processObserving;
    }


    @Override
    public void addSettings(String attribute, String value, String timeRange, String properties, String instanceId)
    {
        //TODO any business process duration logic

        final int partCountProcesses = getBPDPartCountProcesses(Integer.parseInt(timeRange));
        final int partState = getBPDPartState(Integer.parseInt(timeRange));

        setAverageDuration(Integer.parseInt(value));
        setCalculateDuration(partCountProcesses);
        setProcessObserving(getBPDStateItem(partState));

        super.addSettings(attribute, value, timeRange, properties, instanceId);
    }


    @Override
    public SettingsStrategyImpl getSettings()
    {
        //TODO all these are custom based on specific monitor type

        if(null == getAttribute() || getAttribute().isEmpty())
        {
            setAttribute("AverageDurationBPInstances");// TODO different for each monitor type
        }
        if(null == getValue() || getValue().isEmpty())
        {
            setValue(String.valueOf(getAverageDuration()));// TODO different for each monitor type
        }
        if(null == getTimeRange() || getTimeRange().isEmpty())
        {
            final int partCountProcesses = getCalculateDuration();
            final int partState = getBPDStateItemAsInt(getProcessObserving());

            final int timeRange = getBPDFullAttributeName(partState, partCountProcesses);

            setTimeRange(String.valueOf(timeRange));// TODO different for each monitor type
        }
        if(null == getProperties() || getProperties().isEmpty())
        {
            setProperties("");// TODO different for each monitor type - in this case is empty
        }

        return super.getSettings();
    }


    /** Utility logic */
    private int getBPDPartState(final int fullAttributeProperties)
    {
        return fullAttributeProperties & 0x1F;
    }


    private int getBPDPartCountProcesses(final int fullAttributeProperties)
    {
        return fullAttributeProperties >>> 5;
    }

    private int getBPDFullAttributeName(final int partState, final int partCountProcesses)
    {
        return partCountProcesses << 5 | partState;
    }

    private static String getBPDStateItem(final int partState)
    {
        switch(partState)
        {
            case 1:
                return "final";
            case 5:
                return "running";
            default:
                return null;
        }
    }

    private static Integer getBPDStateItemAsInt(final String partState)
    {
        switch(partState)
        {
            case "final":
                return 1;
            case "running":
                return 5;
            default:
                return null;
        }
    }

}
