package com.doncho.monitor;


import com.doncho.monitor.models.AdapterDurationModel;
import com.doncho.monitor.models.BusinessProcessDurationModel;
import com.doncho.monitor.strategy.MonitorDataUtil;
import com.doncho.monitor.strategy.adapter.AdapterStrategyImpl;
import com.doncho.monitor.strategy.businessprocess.ProcessStrategyImpl;
import com.doncho.monitor.strategy.context.MonitorContext;
import com.doncho.monitor.strategy.services.ServicesStrategyImpl;
import com.doncho.monitor.strategy.settings.ISettingsStrategy;
import com.doncho.monitor.strategy.settings.SettingsParent;
import com.doncho.monitor.strategy.settings.SettingsStrategyImpl;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;


public class StrategyMain
{
    public static void main(String[] args)
    {

        // GET method simulation
        /** Step 1 - restResourceId coming from request*/
        /** Step 2 - Monitor node is accessed by restResourceId */
        /** Step 3 - Monitor type and node associations are retrieved from Monitor node */
        /** Step 4 - Based on monitor type can be retrieved adapters, process templates etc. */

        /** Step 5 - Based on previous 4 steps can be filled JAXB root parent {@link com.doncho.monitor.strategy.context.MonitorContext} */
        List<MonitorContext> listContext = new ArrayList<>();
        //Init context
        MonitorContext context = new MonitorContext();

        //Init strategy
        //!Can be used builder pattern instead!
        //context.setSettingsStrategy(new ArrayList<ISettingsStrategy>(Arrays.asList(new BusinessProcessDurationModel())));
        context.setProcessStrategy(new ProcessStrategyImpl());
        context.setAdapterStrategy(new AdapterStrategyImpl());
        context.setServicesStrategy(new ServicesStrategyImpl());

        /** ---------- BusinessProcessDurationMonitor ----------------- */

        //Based on monitor type
        context.setSettingsStrategy(new SettingsParent(new BusinessProcessDurationModel()));

        //fill context
        //!Can be used builder pattern instead!
        context.setBase("BP TEST Monitor", true, null, "BusinessProcessDuration");
        context.setSettings("AverageDurationBPInstances", "77", "32005", "", "Karaf-SP117");
        context.setProcesses(MonitorDataUtil.adaptProcesses());
        context.setServices(MonitorDataUtil.services);

        // Filled with data context - !will be sent as response!
        listContext.add(context);
        System.out.println("/////************* Context: " + context.getName() + " Monitor type: " + context.getMonitorType() + " *********////");
        System.out.println(context.toString() + System.lineSeparator());

        //Marshall and unmarshal
        try
        {
            String fileAddon = context.getMonitorType();

            marshallContext(context, fileAddon);

            /**  POST/PUT methods simulation
             unmarshal method simulates XML data coming by request*/
            context = unmarshalContext(fileAddon);

        }
        catch (JAXBException | FileNotFoundException e)
        {
            e.printStackTrace();
        }

        System.out.println(System.lineSeparator());

        /** ---------- AdapterDurationMonitor ----------------- */

        //Based on monitor type
        context.setSettingsStrategy(new SettingsParent(new AdapterDurationModel()));

        //fill context
        //!Can be used builder pattern instead!
        context.setBase("Adapter duration", false, null, "AdapterDuration");
        context.setSettings("AverageDurationAdapter", "555", "77", "name:Doncho;surname:Balamdzhiev", "*");
        context.setProcesses(null);
        context.setAdapters(MonitorDataUtil.adapters);
        context.setServices(MonitorDataUtil.services);

        // Filled with data context - !will be sent as response!
        listContext.add(context);
        System.out.println("///********** Context: " + context.getName() + " Monitor type: " + context.getMonitorType() + "*****************///");
        System.out.println(context.toString() + System.lineSeparator());


        //Marshall and unmarshal
        try
        {
            String fileAddon = context.getMonitorType();

            marshallContext(context, fileAddon);

            /**  POST/PUT methods simulation
             unmarshal method simulates XML data coming by request*/
            context = unmarshalContext(fileAddon);

        }
        catch (JAXBException | FileNotFoundException e)
        {
            e.printStackTrace();
        }

    }

    private static void marshallContext(MonitorContext monitorContext, String fileAddon) throws JAXBException
    {
        JAXBContext context = JAXBContext.newInstance(MonitorContext.class);
        Marshaller mar= context.createMarshaller();
        mar.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);

        File file = new File("./monitorContext" + fileAddon + ".xml");
        mar.marshal(monitorContext, file);

        System.out.println("=======================");
        System.out.println("Monitor context marshalled to main Dir: " + file.getPath() + System.lineSeparator() +
                           "and File with name: " + file.getName());
    }


    private static MonitorContext unmarshalContext(String fileAddon) throws JAXBException, FileNotFoundException
    {
        JAXBContext context = JAXBContext.newInstance(MonitorContext.class);
        Unmarshaller unmarshaller = context.createUnmarshaller();

        MonitorContext monitorContext = (MonitorContext)unmarshaller.unmarshal(new FileReader("./monitorContext" + fileAddon + ".xml"));

        /** TODO Step 6  Do some validation processes (Check if data from request is valid as has to be)*/

        // Just for test
        // This is your monitor settings values
        monitorContext.getSettingsStrategy();

        System.out.println("=======================");
        System.out.println("Monitor context unmarshalled to Root JAXB class: " + monitorContext.getClass().getName());

        return monitorContext;

    }

}
